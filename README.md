# README #

1/3スケールのNEC PC-6082(DR-320)風小物のstlファイルです。 スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。 元ファイルはAUTODESK FUSION360です。

***

# 実機情報

## メーカ
- NEC

## 発売時期
- 1981年

## 参考資料

- [「僧兵ちまちま」のゲーム日記。](https://blog.goo.ne.jp/timatima-psu/e/8337f1ce13b572bf322110c6784c755e)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_pc-6082/raw/ee4646ebd983d361f3066dd8e0d6f62e61aa9f62/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_pc-6082/raw/ee4646ebd983d361f3066dd8e0d6f62e61aa9f62/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_pc-6082/raw/ee4646ebd983d361f3066dd8e0d6f62e61aa9f62/ExampleImage.png)
